<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'lib/app.php';
$Hobbies ='';
if(array_key_exists('id', $_GET)){
    $Hobbies = findone($_GET['id']);
}
$optBoat='';
$optCoding='';
$optPraying='';
$optGardening='';
$optGyming='';
if(isset($Hobbies) && !empty($Hobbies)){
    if(isset($Hobbies['boat'])=='boat'){
        $optBoat = "Checked='checked'";
    }
    if(isset($Hobbies['code'])=='coding'){
        $optCoding = "Checked='checked'";
    }
    if(isset($Hobbies['pray'])=='praying'){
        $optPraying = "Checked='checked'";
    }
    if(isset($Hobbies['garden'])=='gardening'){
        $optGardening = "Checked='checked'";
    }
    if(isset($Hobbies['gym'])=='gyming'){
        $optGyming = "Checked='checked'";
    }
}
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Checkbox items are selected.</title>
   <meta name="viewport" content="device-width,initial-scale=1.0"
    </head>
    <body>
        <div>
            <nav>
                <ul style="text-align:center">
                    <li> <a href=index.php>List</a></li>
                    <li> <a href="create.php">Add</a></li>
                    <li> <a href="delete.php?id=<?php echo $_GET['id'];?>">Delete</a></li>
                        
                </ul>
            </nav>
        </div>
        <div>
            <dl>
                <dt>Your name<dt>
                <dd><?php echo $Hobbies['fullName']; ?></dd>
            </dl>
        </div>
        <div>
            <fieldset>
                <legend>Your selected Hobbies</legend>
                <dl>
                    <dd>
                        <?php
                        if(isset($Hobbies) && !empty($Hobbies)){
                         ?>
                        <dl>
                        <dd><?php
                                if(isset($Hobbies)&& !empty($Hobbies)){
                                    if (array_key_exists('boat',$Hobbies)){
                                    echo "<p> <input type='checkbox' name='boat' value='boat' <?php echo $optBoat;?>  I enjoy boat journey </p>"; 
                                    }
                                }
                            ?>
                        </dd>
                    <dd>
                        <?php
                        if(isset($Hobbies)&& !empty($Hobbies)){
                            if(array_key_exists('code', $Hobbies)){
                   echo "<p><input type='checkbox' name='code' value='coding'<?php echo $optCoding;?> I enjoy to write codeing </p>";
  
                            }
                        }
                        ?>
                    </dd>
                    <dd>
                        <?php
                        if(isset($Hobbies)&& !empty($Hobbies)){
                            if(array_key_exists('pray', $Hobbies)){
                   echo "<p><input type='checkbox' name='pray' value='praying'<?php echo $optPraying;?> I like to pray my prayers </p>";
  
                            }
                        }
                        ?>
                    </dd>
                    <dd>
                        <?php
                        if(isset($Hobbies)&& !empty($Hobbies)){
                            if(array_key_exists('garden', $Hobbies)){
                   echo "<p><input type='checkbox' name='garden' value='gardening'<?php echo $optGardening;?> I like to make gardining </p>";
  
                            }
                        }
                        ?>
                    </dd>
                         <dd><?php 
                                if(isset($Hobbies)&& !empty($Hobbies)){
                                    if (array_key_exists('gym',$Hobbies)){
                                    echo "<p><input type='checkbox' name='gym' value='gyming' <?php echo $optGyming;?>  I like to gym regularly </p>"; 
                                    }
                                }
                            ?>
                        </dd>
                        <?php
                        }
                        ?>
                    </dd>
                </dl>
            </fieldset>
        </div>
    </body>
</html>
