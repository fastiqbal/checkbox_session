<?php
include_once 'lib/app.php';

if (strtoupper($_SERVER['REQUEST_METHOD']) == 'POST') {
    if(!array_key_exists('hobbies_checkbox', $_SESSION)){
        
        $_SESSION['hobbies_checkbox'] = array();
    }   
    
        
        $_SESSION['hobbies_checkbox'] [] = $_POST;
        header('location:index.php');
        
} else{
    header('location:create.php');
  } 